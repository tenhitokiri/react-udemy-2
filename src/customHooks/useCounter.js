import { useState } from 'react'

const useCounter = (initailCount = 0, value = 1) => {
    const [counter, setCounter] = useState(initailCount)
    const increment = () => {
        setCounter((prevCounter)=>prevCounter +value)
    }    
    const decrement = () => {
        setCounter((prevCounter)=>prevCounter -value)
    }    
    const reset = () => {
        setCounter(0)
    }    
    return [counter, increment, decrement, reset]
}
export default useCounter
