export const increment = (value=1) => {
    return {
        type: 'increment',
        payload: value
    }
}
export const decrement = (value=1) => {
    return {
        type: 'decrement',
        payload: value
    }
}

export const auth = () => {
    return {
        type: 'login'
    }
}

