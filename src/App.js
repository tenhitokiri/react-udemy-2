import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//Components
import './App.css'
import UseMemo from "./components/UseMemo";
import UseRef from "./components/UseRef";
import Counter from "./components/Counter";
import StateReducer from "./components/StateReducer"
import UseContext from "./components/UseContext";
import UseCallback from "./components/UseCallback";
import Hooks from "./components/Hooks";
import Form from "./components/Form"
import About from "./components/About"
import Contacts from "./components/Contacts"
import Home from "./components/Home"
import Navbar from "./components/Navbar"
import Post from "./components/Post"
import PageNotFound from "./components/PageNotFound"
import ProtectedRoute from "./components/ProtectedRoute"

//Redux
import {useSelector, useDispatch} from "react-redux"
import {increment, decrement} from "./actions"


export const NameContext = React.createContext()
export const ColorContext = React.createContext()

function App() {
  const counter = useSelector(state=>state.counter)
  const login = useSelector(state=>state.login)
  const dispatch = useDispatch()

  return (
    <div className="App">
      <Router>
        <div className="Menu">
          <Navbar/>
          <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contacts} />
          <Route path="/hooks" component={Hooks} />
          <Route path="/form" component={Form} />
          <Route path="/callbacks" component={UseCallback} />
          <Route path="/useref" component={UseRef} />
          <ProtectedRoute path="/usememo" component={UseMemo} />
          <ProtectedRoute path="/usecontext" component={UseContext} />
          <ProtectedRoute path="/reducer" component={Counter} />
          <Route path="/statereducer" component={StateReducer} />
          <Route path="/post/:post_id" component={Post} />
          <Route component={PageNotFound} />
          </Switch>
        </div>
      </Router>
      <div className="center">
        <h4>Counter: {counter}</h4>
        <button onClick={() => dispatch(increment())} className="btn btn-success navbar-btn">+</button>
        <button onClick={() => dispatch(decrement())} className="btn btn-danger navbar-btn">-</button>
        <p>{login?<h3>logged in</h3>:<h3>Not logged</h3>}</p>

      </div>
    </div>
  );
}

export default App;