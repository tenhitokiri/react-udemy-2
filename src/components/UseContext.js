import React from 'react'
import ComponentA from "./ComponentA";

export const NameContext = React.createContext()
export const ColorContext = React.createContext()

function UseContext() {

  return (
    <div className="App">
      <div>
        <NameContext.Provider value={'thk'}>
          <ColorContext.Provider value={'Red'}>
            <ComponentA />
          </ColorContext.Provider>
        </NameContext.Provider>
      </div>
    </div>
  );
}

export default UseContext;