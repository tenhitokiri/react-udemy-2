import React from 'react'
import { NavLink, Link, withRouter } from "react-router-dom";
import {useSelector, useDispatch} from "react-redux"
import {auth as reduxAuth} from "../actions/"
import auth from '../auth'

const Navbar = (props) => {
    const login = useSelector(state=>state.login)
    const dispatch = useDispatch()
    const authHandler = () => {
        if(auth.isAuthenticated()){
            auth.logout(() => {
                dispatch(reduxAuth())
                props.history.push('/')
            })
        }
        else{
            auth.login(() => {
                dispatch(reduxAuth())
                props.history.push('/about')
            })
        }
    }
    //const loginText = auth.isAuthenticated() ? 'Logout' : 'Login'
    const loginText = login ? 'Logout' : 'Login'

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
            <Link className="navbar-brand" to="/">Navbar</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="nav nav-tabs mr-auto">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" exact to="/about">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/contact">Contact Us</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/hooks">Hooks</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/form">Form</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/callbacks">Callbacks</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/useref">Use Ref</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/usememo">Use Memo</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/usecontext">Use Context</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/reducer">State Reducer</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/statereducer">State Reducer</NavLink>
                    </li>
                </ul>
                <button className="btn btn-success navbar-btn" onClick={authHandler}>{loginText}</button>
            </div>
        </div>
        </nav>
    )
}

export default withRouter(Navbar)
