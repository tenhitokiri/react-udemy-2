import React from 'react'
import { NameContext, ColorContext } from './UseContext'
const ComponentC = () => {
    return (
        <div>
            <div>
                Component C
            </div>
            <NameContext.Consumer>
                {
                    name => {
                        return (
                            <ColorContext.Consumer>
                            { color => (
                                <div>Name:{name}, color: {color}</div>
                            )
                            }
                            </ColorContext.Consumer>
                        )
                    }
                }
            </NameContext.Consumer>

        </div>
    )
}

export default ComponentC
