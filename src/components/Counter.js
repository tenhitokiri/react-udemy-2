import React , {useReducer} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, ButtonGroup, Badge } from 'reactstrap';

const initialState = {
    counter: 0
}
const reducer = (state,action) =>{
    switch(action.type){
        case 'increment':
            return {...state, counter: state.counter + action.payload}
        case 'decrement':
            return {...state, counter: state.counter - action.payload}
        case 'reset':
            return initialState
        default:
            return state
    }
}

const Counter = () => {
    const [count1, dispatch1] = useReducer(reducer, initialState)
    const [count2, dispatch2] = useReducer(reducer, initialState)
    return (
        <div>
            <ButtonGroup>
                <Button color="primary" outline>
                    Counter1 <Badge color="secondary">{count1.counter}</Badge>
                </Button>
                <Button color="primary" outline>
                    Counter2 <Badge color="secondary">{count2.counter}</Badge>
                </Button>
            </ButtonGroup>
            <p></p>
            <ButtonGroup>
                <Button onClick={()=> dispatch1({type:'increment', payload: 1})} color="dark">Increment by 1</Button>
                <Button onClick={()=> dispatch1({type:'decrement', payload: 1})} color="dark">Decrement by 1</Button>
                <Button onClick={()=> dispatch1({type:'increment', payload: 5})} color="dark">Increment by 5</Button>
                <Button onClick={()=> dispatch1({type:'decrement', payload: 5})} color="dark">Decrement by 5</Button>
                <Button onClick={()=> dispatch1({type:'reset'})} color="danger">Reset</Button>
            </ButtonGroup>
            <p></p>
            <ButtonGroup>
                <Button onClick={()=> dispatch2({type:'increment', payload: 10})} color="dark">Increment 2</Button>
                <Button onClick={()=> dispatch2({type:'decrement', payload: 10})} color="dark">Decrement 2</Button>
                <Button onClick={()=> dispatch2({type:'reset'})} color="danger">Reset</Button>
            </ButtonGroup>
        </div>
    )
}

export default Counter