import React, { useState, useRef, useEffect } from 'react'

const ComponentRef = () => {
    const [count, setCount] = useState(0)
    const componentRef = useRef(true)
    useEffect(() =>{
        return () => {
            componentRef.current = false
        }
    }, [])
    const fakefecth = () => {
        console.log('fake fetch done!')
        setTimeout(() => {
            if(componentRef.current)
            setCount(count +1)
        }, 2000)
    }
    return (
        <div>
            <h1>Component Counter: {count} </h1>
            <button onClick={fakefecth}>Fake Fetch</button>
        </div>
    )
}

export default ComponentRef
