import React , {useContext} from 'react'
import ComponentC from './ComponentC'
import { NameContext, ColorContext } from './UseContext'

const ComponentB = () => {
    const name = useContext(NameContext)
    const color = useContext(ColorContext)
    return (
        <div>
            <div>
                Component B
            </div>
            <div>Name: {name}, color: {color} </div>
            <ComponentC />
        </div>
    )
}

export default ComponentB
