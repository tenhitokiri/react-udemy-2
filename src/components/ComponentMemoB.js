import React , {useEffect} from 'react'
let renderCount = 1

const ComponentMemoB = ({count}) => {
    useEffect(() =>{
        renderCount++;
    })
    return (
        <div>
            <h1>
                Component B : has rendered {renderCount} times, the counter is {count} times
            </h1>

        </div>
    )
}

export default ComponentMemoB
