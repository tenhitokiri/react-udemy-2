import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, ButtonGroup, Badge } from 'reactstrap';
import useCounter from '../customHooks/useCounter';

const ComponentHookA = () => {
    const [count, increment, decrement, reset] = useCounter()
    return (
        <div>
            <ButtonGroup>
                <Button color="primary" outline>
                    Counter A <Badge color="secondary">{count}</Badge>
                </Button>
                <Button onClick={increment} color="dark">Increment by 1</Button>
                <Button onClick={decrement} color="dark">Decrement by 1</Button>
                <Button onClick={reset} color="danger">Reset</Button>
            </ButtonGroup>
            <p></p>
        </div>
    )
}

export default ComponentHookA