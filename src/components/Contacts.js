import React from 'react'
import faker from 'faker'

const Contacts = () => {

    return (
        <div className='container'>
            <h4 className='center'>Contact Us!</h4>
            <p>{ `${faker.lorem.paragraph()}`}</p>
        </div>
    )
}

export default Contacts
