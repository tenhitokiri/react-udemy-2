import React, {useRef, useState} from 'react'
import '../App.css'
import ComponentRef from './ComponentRef'

const UseRef = () => {
  const inputRef = useRef()
  const [show, setShow] = useState(true)
  return (
    <div>
      <h1>Focus on input Field</h1>
      <input ref={inputRef} type="text" />
      <input type="text" />
      <input type="text" />
      <button onClick={() => { inputRef.current.focus()        
      }}>Focus</button>
      <p></p>
      <button onClick={() => { setShow(!show) }}>Toggle Component</button>
      <h1>Unmount components</h1>
      {show && <ComponentRef/>}
    </div>
    
  )
}

export default UseRef
