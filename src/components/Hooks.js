import React from 'react'
import ComponentHookA from './ComponentHookA'
import ComponentHookB from './ComponentHookB'

const Hooks = () => {
    return (
        <div>
        <h2>Hooks</h2>
        <ComponentHookA />
        <ComponentHookB />
        </div>
    )
}

export default Hooks
