import React, {useState, useMemo} from 'react'
import '../App.css'
import ComponentMemoA from './ComponentMemoA'
import ComponentMemoB from './ComponentMemoB'

function UseMemo() {
const [countA, setCountA] = useState(0)
const IncrementA = () => {
    setCountA(countA +1)
}
const memoComponentA = useMemo(()=> {return <ComponentMemoA count={countA}/>}, [countA] )

const [countB, setCountB] = useState(0)
const IncrementB = () => {
    setCountB(countB +1)
}
//const memoComponentB = useMemo(()=> {return <ComponentMemoB count={countB}/>}, [countB] )

  return (
    <div className="App">
        App Js count: {countA}
        <p>
            <button onClick={IncrementA}>Increment A</button>
            <button onClick={IncrementB}>Increment B</button>
        </p>
        {memoComponentA}
        <ComponentMemoB count={countB}/>
    </div>
  );
}

export default UseMemo;