import React, {useEffect, useReducer } from 'react'
import axios from 'axios'
import '../App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { ListGroup, ListGroupItem, Badge } from 'react-bootstrap';

let errortxt = ''
const initialState = {
  loading: true,
  error: "",
  errmsg: "",
  todos: [],

}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_DATA':
      return {
        loading: false,
        error: "",
        errmsg: "",
        todos: action.payload,
      }
        
        case 'SET_ERROR' :
          return {
            loading: false,
            error: "There are some errors",
            errmsg: errortxt,
            todos: [],
          }

      default:
        return state;
  }
}

function StateReducer() {
  const [state, dispatch] = useReducer(reducer, initialState)
  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/todos")
    .then(res =>{
      dispatch({type: 'SET_DATA', payload: res.data})} )
    .catch(e =>{ 
      dispatch({type: 'SET_ERROR'})
    })
  },[])
  const listMarkup = (
    <ListGroup>
      {state.todos.map(todo =><ListGroupItem key={todo.id}>{todo.title} {todo.completed? (<Badge pill variant="success">Completed</Badge>): (<Badge pill variant="danger">Incompleted</Badge>)}</ListGroupItem>)}
    </ListGroup>
  )

  return (
    <div className="App">
      {state.loading? 'Loading...' : (state.error ? state.error :listMarkup)}
    </div>
  );
}

export default StateReducer;