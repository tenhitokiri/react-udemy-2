import React , {useEffect} from 'react'
let renderCount = 1

const ComponentMemoA = ({count}) => {
    useEffect(() =>{
        renderCount++;
    })
    return (
        <div>
            <h1>
                Component A : has rendered {renderCount} times, the counter is {count} times
            </h1>

        </div>
    )
}

export default ComponentMemoA
