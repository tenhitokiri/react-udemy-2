import React, {useCallback , useState} from 'react'
import '../App.css'
const functionCounter = new Set()

const UseCallback = () => {
 const [count1, setCount1] = useState(0)
 const [count2, setCount2] = useState(0)
 const increment = () => {
  console.log('increment')
  setCount1(count1 +1)
  }
 const increment2 = useCallback(() => {
  console.log('increment2')
  setCount2(count2 +1)
}, [count2])

functionCounter.add(increment)
functionCounter.add(increment2)
console.log(functionCounter)
  return (
    <div>
      <p>counter 1 : {count1}; counter 2 : {count2}</p>
      <button onClick={increment}> inc count 1</button>
      <button onClick={increment2}> inc count 2</button>
    </div>
    
  )
}

export default UseCallback
