import React, {useEffect, useState} from 'react'
import axios from 'axios'

const Posts = (props) => {
    const [post, setPost] = useState([])
    const id = props.match.params.post_id
    useEffect(() =>{
        (async () => {
            const res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
            setPost(res.data)
        } )()
    }, [id])
    console.log(props)
    return (
        <div className="card" >
            <div className="card-body">
                <h5 className="card-title">{post.title}</h5>
                <p className="card-text">{post.body}</p>
            </div>
        </div>
    )
}

export default Posts
